<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Relaxation extends Model
{
    public static function RelaxList($count,$order=false)
    {
        //where('sort',true)->
      if($order){
        return static::orderBy($order["name"],$order["method"])->paginate($count);
      }else{
        return static::orderBy('sort','asc')->paginate($count);
      }
    }
    public static function ViewById($id)
    {
      return static::where('id',$id)->first();
    }
}
