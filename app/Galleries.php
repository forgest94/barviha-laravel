<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galleries extends Model
{
    public static function getByRelax($id,$order=false)
    {
        if($order){
            return static::where('id_relax',$id)->orderBy($order["name"],$order["method"])->paginate(0);
          }else{
            return static::where('id_relax',$id)->orderBy('sort','asc')->paginate(0);
          }
    }
    public static function getList($order=false)
    {
        if($order){
            return static::orderBy($order["name"],$order["method"])->paginate(0);
          }else{
            return static::orderBy('sort','asc')->paginate(0);
          }
    }
}
