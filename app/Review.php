<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    public static function ReviewList($count,$order=false)
  {
    if($order){
    return static::orderBy($order["name"],$order["method"])->paginate($count); 
    }else{
    return static::orderBy('sort','asc')->paginate($count);
   }
  }
}
