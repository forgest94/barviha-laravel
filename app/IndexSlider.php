<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class IndexSlider extends Model
{
    public static function Sliders($count,$order=false)
  {
      //where('sort',true)->
    if($order){
      return static::orderBy($order["name"],$order["method"])->paginate($count);
    }else{
      return static::orderBy('sort','asc')->paginate($count);
    }
  }
}
