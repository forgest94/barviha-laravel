<?php

namespace App\Http\Controllers;

use App\Page;
use App\IndexSlider;
use App\Relaxation;
use App\News;
use App\Review;
use App\Galleries;

use Illuminate\Http\Request;

class PageController extends Controller
{
  public function show($slug)
  {
    switch ($slug) {
      case 'news':
        $page = ['title'=>'Новости'];
        $news = News::AllNews(5);
        return view('news',['page'=>$page, 'news'=>$news]);
      break;
      case 'gallery':
        $page = ['title'=>'Фотогалерея'];
        $relax = Relaxation::RelaxList(8);
        $arrGalleryType = array();
        $i=0;
        foreach($relax as $item){
          $gallery = Galleries::getByRelax($item->id);
            if(!empty($gallery)){
              $galleryImg=array();
              foreach($gallery as $G){
                if(!empty($G->img))
                $galleryImg = json_decode($G->img);
              }
              if($galleryImg){
                $arGNew = array_chunk($galleryImg, 6);
                $arrGalleryType[]=array('title'=>$item->title, 'src'=>'/#section'.$i, 'gallery'=>$arGNew);
              }
            }
          $i++;
        }
        return view('gallery',['page'=>$page, 'gallery_type'=>$arrGalleryType]);
      break;
    }
      $page = Page::ViewBySlug($slug);
      return view('page',['page'=>$page]);
  }
  public function index()
  {
      $page = ['title'=>'Главная'];
      $sliders = IndexSlider::Sliders(3);
      $relax = Relaxation::RelaxList(8);
      $reviews = Review::ReviewList(3);
      return view('index',['sliders'=>$sliders, 'page'=>$page, 'relax'=>$relax, 'reviews'=>$reviews]);
  }
}
