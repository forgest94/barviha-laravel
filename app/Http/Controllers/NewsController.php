<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class NewsController extends Controller
{
    public function show($slug)
    {
        $news = News::ViewBySlug($slug);
        $page = ['title'=>'Новость: '.$news->title];
        return view('partials.detailNews',['news'=>$news, 'page'=>$page]);
    }
}
