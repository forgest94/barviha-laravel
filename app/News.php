<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class News extends Model
{
    public static function ViewBySlug($slug)
    {
        return static::where('slug',$slug)->first();
    }
    public static function AllNews($count,$order=false)
  {
    if($order){
    return static::orderBy($order["name"],$order["method"])->paginate($count); 
    }else{
    return static::orderBy('sort','asc')->paginate($count);
   }
  }
}
