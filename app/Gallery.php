<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    public static function getByRelax($id,$order=false)
    {
        if($order){
            return static::where('id_relax',$id)->orderBy($order["name"],$order["method"]);
          }else{
            return static::where('id_relax',$id)->orderBy('sort','asc');
          }
    }
    public static function getList($order=false)
    {
        if($order){
            return static::orderBy($order["name"],$order["method"]);
          }else{
            return static::orderBy('sort','asc');
          }
    }
}
