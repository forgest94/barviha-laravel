'use strict';

(function (root) {

  // svg for all
  svg4everybody();

  function removeHash() {
    location.hash = '';
    history.replaceState(null, null, ' ');
    $(window).scrollTop(scroll);
  }

  function hashChange() {
    var hash = location.hash;

    if (hash) {
      $(hash).find('.offer__more').trigger('click');
    } else {
      $('.featherlight-close').trigger('click');
    }
  }

  var scroll = 0;

  $('.offer__more').featherlight({
    beforeOpen: function beforeOpen() {
      scroll = $(window).scrollTop();
    },
    beforeClose: removeHash
  });

  $('.slider').owlCarousel({
    items: 1
  });

  // Gallery-Page
  $('.g-block__list').each(function (i, gallery) {
    var items = 1;

    if ($(gallery).children().length > items) {
      $(gallery).addClass('owl-carousel');
      $(gallery).owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        dots: true
      });
    }
  });

  // Gallery
  setTimeout(function () {
    $('.gallery__item').on('click', function () {
      var img = $(this).find('img').clone();

      $('.gallery__image').attr('data-featherlight', $(img).attr('src')).html(img);
    });

    hashChange();
  }, 100);

  // Sections
  // $('.offer__more').on('click', function() {
  //   var id = $(this).closest('.section').attr('id'),
  //       bg = $(this).closest('.section').css('background-color');
  //
  //   if (bg === 'rgba(0, 0, 0, 0)') bg = '#bdc7ae';
  //   $('.featherlight').css('background-color', bg);
  //   location.hash = '#'+id;
  //
  //   var itemsGP = $('.gallery__preview').children().length;
  //
  //   if (!$('.gallery__preview').hasClass('owl-carousel') && itemsGP > 6 && $(window).width() > 768) {
  //
  //     $('.gallery__preview').children().each(function(i, item) {
  //
  //       if (i == 0 || i % 6 == 0) {
  //         $('.gallery__preview').append('<div class="gallery__preview-slide"></div>');
  //       }
  //
  //       $('.gallery__preview-slide').last().append(item)
  //     })
  //
  //     $('.gallery__preview').addClass('owl-carousel');
  //     $('.gallery__preview').owlCarousel({
  //       items: 1,
  //       loop: true,
  //       nav: true,
  //       dots: true,
  //       margin: 10
  //     })
  //   } else if (!$('.gallery__preview').hasClass('owl-carousel') && $(window).width() <= 768) {
  //     $('.gallery__preview').addClass('owl-carousel');
  //     $('.gallery__preview').owlCarousel({
  //       items: 2,
  //       loop: true,
  //       nav: true,
  //       dots: true,
  //       margin: 10
  //     })
  //   }
  // })

  // Sections
  $('.offer__more').on('click', function () {
    var bg = $(this).closest('.section').css('background-color');

    if (bg === 'rgba(0, 0, 0, 0)') bg = '#bdc7ae';
    $('.featherlight').css('background-color', bg);
    //   var id = $(this).closest('.section').attr('id'),
    //       bg = $(this).closest('.section').css('background-color');
    //
    //   if (bg === 'rgba(0, 0, 0, 0)') bg = '#bdc7ae';
    //   $('.featherlight').css('background-color', bg);
    //   location.hash = '#' + id;
    //
    //   var itemsGP = $('.gallery__preview').children().length;
    //
    //   if (!$('.gallery__preview').hasClass('owl-carousel') && itemsGP > 6 && $(window).width() > 768) {
    //
    //     $('.gallery__preview').children().each(function (i, item) {
    //
    //       if (i == 0 || i % 6 == 0) {
    //         $('.gallery__preview').append('<div class="gallery__preview-slide"></div>');
    //       }
    //
    //       $('.gallery__preview-slide').last().append(item);
    //     });
    //
    //     $('.gallery__preview').addClass('owl-carousel');
    //     $('.gallery__preview').owlCarousel({
    //       items: 1,
    //       loop: true,
    //       nav: true,
    //       dots: true,
    //       margin: 10
    //     });
    //   }
  });

  $('.header__ico_menu').on('click', function (e) {
    e.preventDefault();

    var fadeBG = '<div class="fadeBG"></div>';

    $('body').append(fadeBG);

    $('.navigator').show();
  });

  $(document).on('click touchend', '.fadeBG', function () {
    $(this).remove();
    $('.navigator').hide();
  });

  if ($(window).width() <= 1000) {
    $('.navigator__list > .navigator__item_drop').on('click', function (e) {

      if ($(this).hasClass('navigator__item_showing')) return;

      var drop = $(this).find('.navigator__drop'),
          title = $(this).find('> a').text();

      $('.navigator__drop-title').remove();
      $(this).addClass('navigator__item_showing');
      $(drop).append('<span class="navigator__drop-title">' + title + '</span>');
    });

    $(document).on('click', '.navigator__drop-title', function (e) {
      e.stopPropagation();

      $('.navigator__item_showing').removeClass('navigator__item_showing');
    });
  }

  $(document).on('click', '.featherlight [data-close]', function (e) {
    e.preventDefault();

    $('.featherlight-close').trigger('click');
  });

  window.onhashchange = hashChange;

  // временно
  // $('.navigator__item_drop').each(function() {
  //   var drop = $(this).find('.navigator__drop');
  //
  //   if (drop.children().length === 0) {
  //     $(this).removeClass('navigator__item_drop');
  //     $(drop).remove()
  //   }
  // })

})(window);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNjcmlwdC5qcyJdLCJuYW1lcyI6WyJyb290Iiwic3ZnNGV2ZXJ5Ym9keSIsInJlbW92ZUhhc2giLCJsb2NhdGlvbiIsImhhc2giLCJoaXN0b3J5IiwicmVwbGFjZVN0YXRlIiwiJCIsIndpbmRvdyIsInNjcm9sbFRvcCIsInNjcm9sbCIsImhhc2hDaGFuZ2UiLCJmaW5kIiwidHJpZ2dlciIsImZlYXRoZXJsaWdodCIsImJlZm9yZU9wZW4iLCJiZWZvcmVDbG9zZSIsIm93bENhcm91c2VsIiwiaXRlbXMiLCJlYWNoIiwiaSIsImdhbGxlcnkiLCJjaGlsZHJlbiIsImxlbmd0aCIsImFkZENsYXNzIiwibG9vcCIsIm5hdiIsImRvdHMiLCJzZXRUaW1lb3V0Iiwib24iLCJpbWciLCJjbG9uZSIsImF0dHIiLCJodG1sIiwiYmciLCJjbG9zZXN0IiwiY3NzIiwiZSIsInByZXZlbnREZWZhdWx0IiwiZmFkZUJHIiwiYXBwZW5kIiwic2hvdyIsImRvY3VtZW50IiwicmVtb3ZlIiwiaGlkZSIsIndpZHRoIiwiaGFzQ2xhc3MiLCJkcm9wIiwidGl0bGUiLCJ0ZXh0Iiwic3RvcFByb3BhZ2F0aW9uIiwicmVtb3ZlQ2xhc3MiLCJvbmhhc2hjaGFuZ2UiXSwibWFwcGluZ3MiOiI7O0FBQUEsQ0FBQyxVQUFTQSxJQUFULEVBQWU7O0FBRWQ7QUFDQUM7O0FBRUEsV0FBU0MsVUFBVCxHQUF1QjtBQUNyQkMsYUFBU0MsSUFBVCxHQUFnQixFQUFoQjtBQUNBQyxZQUFRQyxZQUFSLENBQXFCLElBQXJCLEVBQTJCLElBQTNCLEVBQWlDLEdBQWpDO0FBQ0FDLE1BQUVDLE1BQUYsRUFBVUMsU0FBVixDQUFvQkMsTUFBcEI7QUFDRDs7QUFFRCxXQUFTQyxVQUFULEdBQXNCO0FBQ3BCLFFBQUlQLE9BQU9ELFNBQVNDLElBQXBCOztBQUVBLFFBQUlBLElBQUosRUFBVTtBQUNSRyxRQUFFSCxJQUFGLEVBQVFRLElBQVIsQ0FBYSxjQUFiLEVBQTZCQyxPQUE3QixDQUFxQyxPQUFyQztBQUNELEtBRkQsTUFFTztBQUNMTixRQUFFLHFCQUFGLEVBQXlCTSxPQUF6QixDQUFpQyxPQUFqQztBQUNEO0FBQ0Y7O0FBRUQsTUFBSUgsU0FBUyxDQUFiOztBQUVBSCxJQUFFLGNBQUYsRUFBa0JPLFlBQWxCLENBQStCO0FBQzdCQyxnQkFBWSxzQkFBVztBQUNyQkwsZUFBU0gsRUFBRUMsTUFBRixFQUFVQyxTQUFWLEVBQVQ7QUFDRCxLQUg0QjtBQUk3Qk8saUJBQWFkO0FBSmdCLEdBQS9COztBQU9BSyxJQUFFLFNBQUYsRUFBYVUsV0FBYixDQUF5QjtBQUN2QkMsV0FBTztBQURnQixHQUF6Qjs7QUFJQTtBQUNBWCxJQUFFLGdCQUFGLEVBQW9CWSxJQUFwQixDQUF5QixVQUFTQyxDQUFULEVBQVlDLE9BQVosRUFBcUI7QUFDNUMsUUFBSUgsUUFBUSxDQUFaOztBQUVBLFFBQUlYLEVBQUVjLE9BQUYsRUFBV0MsUUFBWCxHQUFzQkMsTUFBdEIsR0FBK0JMLEtBQW5DLEVBQTBDO0FBQ3hDWCxRQUFFYyxPQUFGLEVBQVdHLFFBQVgsQ0FBb0IsY0FBcEI7QUFDQWpCLFFBQUVjLE9BQUYsRUFBV0osV0FBWCxDQUF1QjtBQUNyQkMsZUFBTyxDQURjO0FBRXJCTyxjQUFNLElBRmU7QUFHckJDLGFBQUssSUFIZ0I7QUFJckJDLGNBQU07QUFKZSxPQUF2QjtBQU1EO0FBQ0YsR0FaRDs7QUFjQTtBQUNBQyxhQUFXLFlBQVc7QUFDcEJyQixNQUFFLGdCQUFGLEVBQW9Cc0IsRUFBcEIsQ0FBdUIsT0FBdkIsRUFBZ0MsWUFBVztBQUN6QyxVQUFJQyxNQUFNdkIsRUFBRSxJQUFGLEVBQVFLLElBQVIsQ0FBYSxLQUFiLEVBQW9CbUIsS0FBcEIsRUFBVjs7QUFFQXhCLFFBQUUsaUJBQUYsRUFBcUJ5QixJQUFyQixDQUEwQixtQkFBMUIsRUFBK0N6QixFQUFFdUIsR0FBRixFQUFPRSxJQUFQLENBQVksS0FBWixDQUEvQyxFQUFtRUMsSUFBbkUsQ0FBd0VILEdBQXhFO0FBQ0QsS0FKRDs7QUFNQW5CO0FBQ0QsR0FSRCxFQVFHLEdBUkg7O0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBSixJQUFFLGNBQUYsRUFBa0JzQixFQUFsQixDQUFxQixPQUFyQixFQUE4QixZQUFZO0FBQ3hDLFFBQUlLLEtBQUszQixFQUFFLElBQUYsRUFBUTRCLE9BQVIsQ0FBZ0IsVUFBaEIsRUFBNEJDLEdBQTVCLENBQWdDLGtCQUFoQyxDQUFUOztBQUVBLFFBQUlGLE9BQU8sa0JBQVgsRUFBK0JBLEtBQUssU0FBTDtBQUMvQjNCLE1BQUUsZUFBRixFQUFtQjZCLEdBQW5CLENBQXVCLGtCQUF2QixFQUEyQ0YsRUFBM0M7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0MsR0FsQ0Q7O0FBb0NBM0IsSUFBRSxtQkFBRixFQUF1QnNCLEVBQXZCLENBQTBCLE9BQTFCLEVBQW1DLFVBQVNRLENBQVQsRUFBWTtBQUM3Q0EsTUFBRUMsY0FBRjs7QUFFQSxRQUFJQyxTQUFTLDRCQUFiOztBQUVBaEMsTUFBRSxNQUFGLEVBQVVpQyxNQUFWLENBQWlCRCxNQUFqQjs7QUFFQWhDLE1BQUUsWUFBRixFQUFnQmtDLElBQWhCO0FBQ0QsR0FSRDs7QUFVQWxDLElBQUVtQyxRQUFGLEVBQVliLEVBQVosQ0FBZSxPQUFmLEVBQXdCLFNBQXhCLEVBQW1DLFlBQVc7QUFDNUN0QixNQUFFLElBQUYsRUFBUW9DLE1BQVI7QUFDQXBDLE1BQUUsWUFBRixFQUFnQnFDLElBQWhCO0FBQ0QsR0FIRDs7QUFLQSxNQUFJckMsRUFBRUMsTUFBRixFQUFVcUMsS0FBVixNQUFxQixJQUF6QixFQUErQjtBQUM3QnRDLE1BQUUsdUJBQUYsRUFBMkJzQixFQUEzQixDQUE4QixPQUE5QixFQUF1QyxVQUFTUSxDQUFULEVBQVk7QUFDakRBLFFBQUVDLGNBQUY7O0FBRUEsVUFBSS9CLEVBQUUsSUFBRixFQUFRdUMsUUFBUixDQUFpQix5QkFBakIsQ0FBSixFQUFpRDs7QUFFakQsVUFBSUMsT0FBT3hDLEVBQUUsSUFBRixFQUFRSyxJQUFSLENBQWEsa0JBQWIsQ0FBWDtBQUFBLFVBQ0lvQyxRQUFRekMsRUFBRSxJQUFGLEVBQVFLLElBQVIsQ0FBYSxLQUFiLEVBQW9CcUMsSUFBcEIsRUFEWjs7QUFHQTFDLFFBQUUsd0JBQUYsRUFBNEJvQyxNQUE1QjtBQUNBcEMsUUFBRSxJQUFGLEVBQVFpQixRQUFSLENBQWlCLHlCQUFqQjtBQUNBakIsUUFBRXdDLElBQUYsRUFBUVAsTUFBUixDQUFlLHlDQUF1Q1EsS0FBdkMsR0FBNkMsU0FBNUQ7QUFDRCxLQVhEOztBQWFBekMsTUFBRW1DLFFBQUYsRUFBWWIsRUFBWixDQUFlLE9BQWYsRUFBd0Isd0JBQXhCLEVBQWtELFVBQVNRLENBQVQsRUFBWTtBQUM1REEsUUFBRWEsZUFBRjs7QUFFQTNDLFFBQUUsMEJBQUYsRUFBOEI0QyxXQUE5QixDQUEwQyx5QkFBMUM7QUFDRCxLQUpEO0FBS0Q7O0FBRUQ1QyxJQUFFbUMsUUFBRixFQUFZYixFQUFaLENBQWUsT0FBZixFQUF3Qiw0QkFBeEIsRUFBc0QsVUFBU1EsQ0FBVCxFQUFZO0FBQ2hFQSxNQUFFQyxjQUFGOztBQUVBL0IsTUFBRSxxQkFBRixFQUF5Qk0sT0FBekIsQ0FBaUMsT0FBakM7QUFDRCxHQUpEOztBQU1BTCxTQUFPNEMsWUFBUCxHQUFzQnpDLFVBQXRCO0FBSUQsQ0F6TEQsRUF5TEdILE1BekxIIiwiZmlsZSI6InNjcmlwdC5qcyIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbihyb290KSB7XG5cbiAgLy8gc3ZnIGZvciBhbGxcbiAgc3ZnNGV2ZXJ5Ym9keSgpO1xuXG4gIGZ1bmN0aW9uIHJlbW92ZUhhc2ggKCkge1xuICAgIGxvY2F0aW9uLmhhc2ggPSAnJztcbiAgICBoaXN0b3J5LnJlcGxhY2VTdGF0ZShudWxsLCBudWxsLCAnICcpO1xuICAgICQod2luZG93KS5zY3JvbGxUb3Aoc2Nyb2xsKVxuICB9XG5cbiAgZnVuY3Rpb24gaGFzaENoYW5nZSgpIHtcbiAgICB2YXIgaGFzaCA9IGxvY2F0aW9uLmhhc2g7XG5cbiAgICBpZiAoaGFzaCkge1xuICAgICAgJChoYXNoKS5maW5kKCcub2ZmZXJfX21vcmUnKS50cmlnZ2VyKCdjbGljaycpXG4gICAgfSBlbHNlIHtcbiAgICAgICQoJy5mZWF0aGVybGlnaHQtY2xvc2UnKS50cmlnZ2VyKCdjbGljaycpO1xuICAgIH1cbiAgfVxuXG4gIHZhciBzY3JvbGwgPSAwO1xuXG4gICQoJy5vZmZlcl9fbW9yZScpLmZlYXRoZXJsaWdodCh7XG4gICAgYmVmb3JlT3BlbjogZnVuY3Rpb24oKSB7XG4gICAgICBzY3JvbGwgPSAkKHdpbmRvdykuc2Nyb2xsVG9wKCk7XG4gICAgfSxcbiAgICBiZWZvcmVDbG9zZTogcmVtb3ZlSGFzaFxuICB9KVxuXG4gICQoJy5zbGlkZXInKS5vd2xDYXJvdXNlbCh7XG4gICAgaXRlbXM6IDFcbiAgfSlcblxuICAvLyBHYWxsZXJ5LVBhZ2VcbiAgJCgnLmctYmxvY2tfX2xpc3QnKS5lYWNoKGZ1bmN0aW9uKGksIGdhbGxlcnkpIHtcbiAgICB2YXIgaXRlbXMgPSAxO1xuXG4gICAgaWYgKCQoZ2FsbGVyeSkuY2hpbGRyZW4oKS5sZW5ndGggPiBpdGVtcykge1xuICAgICAgJChnYWxsZXJ5KS5hZGRDbGFzcygnb3dsLWNhcm91c2VsJyk7XG4gICAgICAkKGdhbGxlcnkpLm93bENhcm91c2VsKHtcbiAgICAgICAgaXRlbXM6IDEsXG4gICAgICAgIGxvb3A6IHRydWUsXG4gICAgICAgIG5hdjogdHJ1ZSxcbiAgICAgICAgZG90czogdHJ1ZVxuICAgICAgfSlcbiAgICB9XG4gIH0pO1xuXG4gIC8vIEdhbGxlcnlcbiAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAkKCcuZ2FsbGVyeV9faXRlbScpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIGltZyA9ICQodGhpcykuZmluZCgnaW1nJykuY2xvbmUoKTtcblxuICAgICAgJCgnLmdhbGxlcnlfX2ltYWdlJykuYXR0cignZGF0YS1mZWF0aGVybGlnaHQnLCAkKGltZykuYXR0cignc3JjJykpLmh0bWwoaW1nKTtcbiAgICB9KTtcblxuICAgIGhhc2hDaGFuZ2UoKTtcbiAgfSwgMTAwKVxuXG4gIC8vIFNlY3Rpb25zXG4gIC8vICQoJy5vZmZlcl9fbW9yZScpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuICAvLyAgIHZhciBpZCA9ICQodGhpcykuY2xvc2VzdCgnLnNlY3Rpb24nKS5hdHRyKCdpZCcpLFxuICAvLyAgICAgICBiZyA9ICQodGhpcykuY2xvc2VzdCgnLnNlY3Rpb24nKS5jc3MoJ2JhY2tncm91bmQtY29sb3InKTtcbiAgLy9cbiAgLy8gICBpZiAoYmcgPT09ICdyZ2JhKDAsIDAsIDAsIDApJykgYmcgPSAnI2JkYzdhZSc7XG4gIC8vICAgJCgnLmZlYXRoZXJsaWdodCcpLmNzcygnYmFja2dyb3VuZC1jb2xvcicsIGJnKTtcbiAgLy8gICBsb2NhdGlvbi5oYXNoID0gJyMnK2lkO1xuICAvL1xuICAvLyAgIHZhciBpdGVtc0dQID0gJCgnLmdhbGxlcnlfX3ByZXZpZXcnKS5jaGlsZHJlbigpLmxlbmd0aDtcbiAgLy9cbiAgLy8gICBpZiAoISQoJy5nYWxsZXJ5X19wcmV2aWV3JykuaGFzQ2xhc3MoJ293bC1jYXJvdXNlbCcpICYmIGl0ZW1zR1AgPiA2ICYmICQod2luZG93KS53aWR0aCgpID4gNzY4KSB7XG4gIC8vXG4gIC8vICAgICAkKCcuZ2FsbGVyeV9fcHJldmlldycpLmNoaWxkcmVuKCkuZWFjaChmdW5jdGlvbihpLCBpdGVtKSB7XG4gIC8vXG4gIC8vICAgICAgIGlmIChpID09IDAgfHwgaSAlIDYgPT0gMCkge1xuICAvLyAgICAgICAgICQoJy5nYWxsZXJ5X19wcmV2aWV3JykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiZ2FsbGVyeV9fcHJldmlldy1zbGlkZVwiPjwvZGl2PicpO1xuICAvLyAgICAgICB9XG4gIC8vXG4gIC8vICAgICAgICQoJy5nYWxsZXJ5X19wcmV2aWV3LXNsaWRlJykubGFzdCgpLmFwcGVuZChpdGVtKVxuICAvLyAgICAgfSlcbiAgLy9cbiAgLy8gICAgICQoJy5nYWxsZXJ5X19wcmV2aWV3JykuYWRkQ2xhc3MoJ293bC1jYXJvdXNlbCcpO1xuICAvLyAgICAgJCgnLmdhbGxlcnlfX3ByZXZpZXcnKS5vd2xDYXJvdXNlbCh7XG4gIC8vICAgICAgIGl0ZW1zOiAxLFxuICAvLyAgICAgICBsb29wOiB0cnVlLFxuICAvLyAgICAgICBuYXY6IHRydWUsXG4gIC8vICAgICAgIGRvdHM6IHRydWUsXG4gIC8vICAgICAgIG1hcmdpbjogMTBcbiAgLy8gICAgIH0pXG4gIC8vICAgfSBlbHNlIGlmICghJCgnLmdhbGxlcnlfX3ByZXZpZXcnKS5oYXNDbGFzcygnb3dsLWNhcm91c2VsJykgJiYgJCh3aW5kb3cpLndpZHRoKCkgPD0gNzY4KSB7XG4gIC8vICAgICAkKCcuZ2FsbGVyeV9fcHJldmlldycpLmFkZENsYXNzKCdvd2wtY2Fyb3VzZWwnKTtcbiAgLy8gICAgICQoJy5nYWxsZXJ5X19wcmV2aWV3Jykub3dsQ2Fyb3VzZWwoe1xuICAvLyAgICAgICBpdGVtczogMixcbiAgLy8gICAgICAgbG9vcDogdHJ1ZSxcbiAgLy8gICAgICAgbmF2OiB0cnVlLFxuICAvLyAgICAgICBkb3RzOiB0cnVlLFxuICAvLyAgICAgICBtYXJnaW46IDEwXG4gIC8vICAgICB9KVxuICAvLyAgIH1cbiAgLy8gfSlcblxuICAvLyBTZWN0aW9uc1xuICAkKCcub2ZmZXJfX21vcmUnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGJnID0gJCh0aGlzKS5jbG9zZXN0KCcuc2VjdGlvbicpLmNzcygnYmFja2dyb3VuZC1jb2xvcicpO1xuXG4gICAgaWYgKGJnID09PSAncmdiYSgwLCAwLCAwLCAwKScpIGJnID0gJyNiZGM3YWUnO1xuICAgICQoJy5mZWF0aGVybGlnaHQnKS5jc3MoJ2JhY2tncm91bmQtY29sb3InLCBiZyk7XG4gIC8vICAgdmFyIGlkID0gJCh0aGlzKS5jbG9zZXN0KCcuc2VjdGlvbicpLmF0dHIoJ2lkJyksXG4gIC8vICAgICAgIGJnID0gJCh0aGlzKS5jbG9zZXN0KCcuc2VjdGlvbicpLmNzcygnYmFja2dyb3VuZC1jb2xvcicpO1xuICAvL1xuICAvLyAgIGlmIChiZyA9PT0gJ3JnYmEoMCwgMCwgMCwgMCknKSBiZyA9ICcjYmRjN2FlJztcbiAgLy8gICAkKCcuZmVhdGhlcmxpZ2h0JykuY3NzKCdiYWNrZ3JvdW5kLWNvbG9yJywgYmcpO1xuICAvLyAgIGxvY2F0aW9uLmhhc2ggPSAnIycgKyBpZDtcbiAgLy9cbiAgLy8gICB2YXIgaXRlbXNHUCA9ICQoJy5nYWxsZXJ5X19wcmV2aWV3JykuY2hpbGRyZW4oKS5sZW5ndGg7XG4gIC8vXG4gIC8vICAgaWYgKCEkKCcuZ2FsbGVyeV9fcHJldmlldycpLmhhc0NsYXNzKCdvd2wtY2Fyb3VzZWwnKSAmJiBpdGVtc0dQID4gNiAmJiAkKHdpbmRvdykud2lkdGgoKSA+IDc2OCkge1xuICAvL1xuICAvLyAgICAgJCgnLmdhbGxlcnlfX3ByZXZpZXcnKS5jaGlsZHJlbigpLmVhY2goZnVuY3Rpb24gKGksIGl0ZW0pIHtcbiAgLy9cbiAgLy8gICAgICAgaWYgKGkgPT0gMCB8fCBpICUgNiA9PSAwKSB7XG4gIC8vICAgICAgICAgJCgnLmdhbGxlcnlfX3ByZXZpZXcnKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJnYWxsZXJ5X19wcmV2aWV3LXNsaWRlXCI+PC9kaXY+Jyk7XG4gIC8vICAgICAgIH1cbiAgLy9cbiAgLy8gICAgICAgJCgnLmdhbGxlcnlfX3ByZXZpZXctc2xpZGUnKS5sYXN0KCkuYXBwZW5kKGl0ZW0pO1xuICAvLyAgICAgfSk7XG4gIC8vXG4gIC8vICAgICAkKCcuZ2FsbGVyeV9fcHJldmlldycpLmFkZENsYXNzKCdvd2wtY2Fyb3VzZWwnKTtcbiAgLy8gICAgICQoJy5nYWxsZXJ5X19wcmV2aWV3Jykub3dsQ2Fyb3VzZWwoe1xuICAvLyAgICAgICBpdGVtczogMSxcbiAgLy8gICAgICAgbG9vcDogdHJ1ZSxcbiAgLy8gICAgICAgbmF2OiB0cnVlLFxuICAvLyAgICAgICBkb3RzOiB0cnVlLFxuICAvLyAgICAgICBtYXJnaW46IDEwXG4gIC8vICAgICB9KTtcbiAgLy8gICB9XG4gIH0pO1xuXG4gICQoJy5oZWFkZXJfX2ljb19tZW51Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSkge1xuICAgIGUucHJldmVudERlZmF1bHQoKTtcblxuICAgIHZhciBmYWRlQkcgPSAnPGRpdiBjbGFzcz1cImZhZGVCR1wiPjwvZGl2Pic7XG5cbiAgICAkKCdib2R5JykuYXBwZW5kKGZhZGVCRyk7XG5cbiAgICAkKCcubmF2aWdhdG9yJykuc2hvdygpO1xuICB9KVxuXG4gICQoZG9jdW1lbnQpLm9uKCdjbGljaycsICcuZmFkZUJHJywgZnVuY3Rpb24oKSB7XG4gICAgJCh0aGlzKS5yZW1vdmUoKTtcbiAgICAkKCcubmF2aWdhdG9yJykuaGlkZSgpO1xuICB9KVxuXG4gIGlmICgkKHdpbmRvdykud2lkdGgoKSA8PSAxMDAwKSB7XG4gICAgJCgnLm5hdmlnYXRvcl9faXRlbV9kcm9wJykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSkge1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICBpZiAoJCh0aGlzKS5oYXNDbGFzcygnbmF2aWdhdG9yX19pdGVtX3Nob3dpbmcnKSkgcmV0dXJuXG5cbiAgICAgIHZhciBkcm9wID0gJCh0aGlzKS5maW5kKCcubmF2aWdhdG9yX19kcm9wJyksXG4gICAgICAgICAgdGl0bGUgPSAkKHRoaXMpLmZpbmQoJz4gYScpLnRleHQoKTtcblxuICAgICAgJCgnLm5hdmlnYXRvcl9fZHJvcC10aXRsZScpLnJlbW92ZSgpO1xuICAgICAgJCh0aGlzKS5hZGRDbGFzcygnbmF2aWdhdG9yX19pdGVtX3Nob3dpbmcnKTtcbiAgICAgICQoZHJvcCkuYXBwZW5kKCc8c3BhbiBjbGFzcz1cIm5hdmlnYXRvcl9fZHJvcC10aXRsZVwiPicrdGl0bGUrJzwvc3Bhbj4nKVxuICAgIH0pO1xuXG4gICAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgJy5uYXZpZ2F0b3JfX2Ryb3AtdGl0bGUnLCBmdW5jdGlvbihlKSB7XG4gICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuXG4gICAgICAkKCcubmF2aWdhdG9yX19pdGVtX3Nob3dpbmcnKS5yZW1vdmVDbGFzcygnbmF2aWdhdG9yX19pdGVtX3Nob3dpbmcnKTtcbiAgICB9KTtcbiAgfVxuXG4gICQoZG9jdW1lbnQpLm9uKCdjbGljaycsICcuZmVhdGhlcmxpZ2h0IFtkYXRhLWNsb3NlXScsIGZ1bmN0aW9uKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAkKCcuZmVhdGhlcmxpZ2h0LWNsb3NlJykudHJpZ2dlcignY2xpY2snKTtcbiAgfSlcblxuICB3aW5kb3cub25oYXNoY2hhbmdlID0gaGFzaENoYW5nZVxuXG5cblxufSkod2luZG93KTtcbiJdfQ==
