$(document).ready(function() {
    $('.offer__more').on('click',function(){
        var id = $(this).data('idrelax'), class_modal=$(this).data('classmoadl');
        $.ajax({
            url: '/detailRelax',
            type: "POST",
            data: {id:id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $(document).find('.offer_full').html(data);
                $(document).find('.offer_full').addClass(class_modal);
                $(document).find('.offer_full').attr('data-class',class_modal);

                location.hash = '#'+id;

                var itemsGP = $('.gallery__preview').children().length;

                if (!$('.gallery__preview').hasClass('owl-carousel') && itemsGP > 6 && $(window).width() > 768) {

                  $('.gallery__preview').children().each(function(i, item) {

                    if (i == 0 || i % 6 == 0) {
                      $('.gallery__preview').append('<div class="gallery__preview-slide"></div>');
                    }

                    $('.gallery__preview-slide').last().append(item)
                  })

                  $('.gallery__preview').addClass('owl-carousel');
                  $('.gallery__preview').owlCarousel({
                    items: 1,
                    loop: true,
                    nav: true,
                    dots: true,
                    margin: 10
                  })
                } else if (!$('.gallery__preview').hasClass('owl-carousel') && $(window).width() <= 768) {
                  $('.gallery__preview').addClass('owl-carousel');
                  $('.gallery__preview').owlCarousel({
                    items: 2,
                    loop: true,
                    nav: true,
                    dots: true,
                    margin: 10
                  })
                }
            }
        });
    });

    $('.call-btn').on('click',function(){
       $(document).find('[name="typerelax"]').val($(this).data('namercall'));
    });
    $(document).on('submit','#call-form',function(e){
        e.preventDefault();
        var $that = $(this),
             formData = new FormData($that.get(0));
             var errors = '';
             $that.find('.req').each(function(i,elem){
                if($(elem).val() == ''){
                    errors = 'Y';
                    $(elem).css('border','1px solid #f00');
                }else{
                    $(elem).css('border','2px solid #c0d1b3');
                }
             });
        if(!errors){
            $.ajax({
                url: '/mailCall',
                type: "post",
                contentType: false,
                processData: false,
                data: formData,
                headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $(document).find('.callback__content').html('<p>'+data+'</p>');
                }
            });
        }
    });
    $(document).on('click', '[aria-label="Close"]',function(){
        var remClass = $(this).parent().find('.offer_full').data('class');
        $(document).find('#offer').removeClass(remClass);
        $(this).parent().find('.offer_full').html('');
        $(this).parent().find('[name="type-relax"]').val('');
    });
});
