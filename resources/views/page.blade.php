@extends('layouts.template')
@section('content')
<section class="section">
      <h2 class="caption">
      {!! $page->title !!}
      </h2>
      <div class="textpage">
        <div class="textpage__container container">
          <div class="content">
          {!! $page->body !!}
          </div>
        </div>
      </div>
</section>
@endsection