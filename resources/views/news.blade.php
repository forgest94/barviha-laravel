@extends('layouts.template')
@section('content')
<section class="section">
      <div class="section__container container">
        <h2 class="caption">
            {!! $page['title'] !!}
        </h2>
        <?
        $i=0;
        ?>
        @foreach($news as $itemNew)
        <?
        $i++;
        $galleryItem = '';
        if(!empty($itemNew->gallery))
        $galleryItem = json_decode($itemNew->gallery);
        ?>
          <article class="article">
            <a class="article__title" href="/news/{!! $itemNew->slug !!}/">{!! $itemNew->title !!}</a>
            <div class="content">
              {!! $itemNew->preview_text !!}   
            </div>
            @if(!empty($galleryItem))
              @if(count($galleryItem) == 1)
              <div class="article__images article__images_single">
              @else
              <div class="article__images">
              @endif
                @foreach($galleryItem as $pic)
                  <img src="{!! Voyager::image($pic) !!}">
                @endforeach
              </div>
            @endif
            <div class="article__footer">
            <div class="social">
            <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
            <script src="https://yastatic.net/share2/share.js"></script>
            <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter,viber,whatsapp,skype,telegram" data-limit="3" data-url="http://barviha.na4u.ru/news/<?=$itemNew->slug?>/" data-title="<?=$itemNew->title;?>" <?if(!empty($galleryItem) && count($galleryItem) >= 1):?>data-image="{!! Voyager::image($galleryItem[0]) !!}"<?endif;?>></div>
              <!--<a href="#"><img src="../img/icons/ok.png" alt="Одноклассники"/></a>
              <a href="#"><img src="../img/icons/vk.png" alt="Вконтакте"/></a>
              <a href="#"><img src="../img/icons/insta.png" alt="Instagram"/></a>-->
            </div><a class="call-btn" href="/news/{!! $itemNew->slug !!}/">Подробнее</a>
            </div>
            <p class="article__publish">
              Опубликовано {{ \Carbon\Carbon::parse($itemNew->created_at)->format('d.m.Y') }}
            </p>
          </article>
        @endforeach
        {{ $news->links('partials.nav') }}
</div>
</section>
@endsection