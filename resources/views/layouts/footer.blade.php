    <footer class="page__footer">
      <footer class="footer">
        <div class="footer__container container">
        {{ menu('footer-menu','partials.footerMenu') }}
          <div class="footer__end">
            <div class="address">
            {!! setting('site.cotnacts_footer') !!}
            </div>
            <div class="social">
            {!! setting('site.social_footer') !!}
            </div>
          </div>
        </div>
      </footer>

      <script src="/js/libs.js"></script>
      <script src="/js/script.js"></script>
      <script src="/js/fix.js"></script>
      <script src="/js/maps.js"></script>
    </footer>
