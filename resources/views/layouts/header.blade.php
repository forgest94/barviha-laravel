<header class="page__header">
      <div class="header">
        <div class="header__container container">
          <div class="header__main">
            <div class="header__block"><a class="logotype" href="/"><img class="logotype__img" src="{!! Voyager::image(setting('site.logo')) !!}" alt="{!! Voyager::setting('site.title') !!}" title=""/></a>
            </div>
            <div class="header__block">
              <h1 class="title">
                <a href="/"><img src="{!! Voyager::image(setting('site.baner_top')) !!}" alt="" role="presentation"/>
                <p>{!! setting('site.text_baner_top') !!}</p>
                </a>
              </h1>
            </div>
            <div class="header__block">
              <div class="address">
                {!! setting('site.cotnacts_header') !!}
                <button class="call-btn call-btn_header" data-namercall="Форма из шапки" data-featherlight="#callback">Заказать звонок
                </button>
              </div>
            </div>
            <div class="header__mob"><a class="header__ico header__ico_tel" href="tel:83852606012"></a><a class="header__ico header__ico_geo" href="#map"></a>
              <div class="header__ico header__ico_menu">
              </div>
            </div>
          </div>
          {{ menu('top-menu','partials.topMenu') }}
        </div>
      </div>
    </header>
