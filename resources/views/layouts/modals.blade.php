<div class="page__hidden">
      <div class="offer offer_full" id="offer" data-class="">

      </div>
      <div class="callback" id="callback">
        <div class="callback__content"><span class="caption caption_light caption_strong">Заказать звонок</span>
          <div class="callback__text">
            <p>Бронирование круглосуточно: +7(3852)60-60-12<br>info@barviha.ru</p>
            <p>База отдыха Барвиха ул. Власихинская, 67А / 477</p>
            <p>Заполните, пожалуйста, форму ниже. Администратор комплекса свяжется с Вами в ближайшее время.</p>
          </div>
          <form class="form" id="call-form">
            <div class="form__line">
              <label class="field field_required">
                <div class="field__input"><input class="req" type="text" name="name" placeholder="Ваше имя (обязательно):"/>
                </div>
              </label>
            </div>
            <div class="form__line">
              <label class="field field_required">
                <div class="field__input"><input class="req" type="tel" name="mail" placeholder="Номер телефона (обязательно):"/>
                </div>
              </label>
            </div>
            <div class="form__line">
              <label class="field field_text">
                <div class="field__input"><textarea type="text" name="text" placeholder="Ваш комментарий (по желанию):"></textarea>
                </div>
              </label>
            </div>
            <input type="hidden" name="typerelax" value="">
            <div class="form__submit"><input class="call-btn" type="submit"/>
            </div>
          </form>
        </div>
      </div>
    </div>
