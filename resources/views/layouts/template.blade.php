<!DOCTYPE html>
<html>
  <meta charset="UTF-8"/>
  <title>{!! Voyager::setting('site.title') !!} — {{ $page['title']??$page->title }}</title>
  <link rel="stylesheet" href="/css/style.css"/>
  <link rel="stylesheet" href="/css/fix.css"/>
  <link rel="icon" type="image/x-icon" href="{!! Voyager::image(setting('site.icons')) !!}"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
  <script src="https://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>
  <meta name="csrf-token" content="{{ csrf_token() }}"/>
  <body class="page">
    @include('layouts.header')
    @yield('content')
    @include('layouts.footer')
    @include('layouts.modals')
  </body>
</html>