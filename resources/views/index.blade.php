
@extends('layouts.template')
@section('content')
<section class="section section_welcome">
      <div class="slider owl-carousel">
          @foreach($sliders as $slide)
          <img src="{!! Voyager::image($slide->images) !!}" alt="{!! $slide->name !!}" role="presentation"/>
          @endforeach
      </div>
      <div class="section__container container">
      {!! Voyager::setting('site.text_index') !!}
      <ul class="offers-list">
      <?$i=0;?>
      @foreach($relax as $relaxItem)
          <li class="offer-link"><a href="#section{!! $i !!}">
            <div class="offer-link__ico"><img src="{!! Voyager::image($relaxItem->icon) !!}" alt="{!! $relaxItem->title !!}"/>
            </div><span class="offer-link__name">{!! $relaxItem->title !!}</span></a>
          </li>
      <?$i++;?>
      @endforeach
          <li class="offer-link offer-link_disabled">
            <div class="offer-link__ico"><img src="img/icons/coming.png" alt="Скоро! Новое место отдыха"/>
            </div><span class="offer-link__name">Скоро! Новое место отдыха</span>
          </li>
        </ul>
      </div>
    </section>
    <?$i=0;$i2=1;?>
    @foreach($relax as $relaxItem)
    <section class="section" id="section{!! $i !!}" style="background:{!! $relaxItem->color !!};">
      <div class="section__container container">
        @if($i2%3 && $i2!=1)
        <div class="offer offer_style-odd" style="background:{!! $relaxItem->color !!};">
        @else
        <div class="offer">
        @endif
          <div class="offer__bg" style="background-image:url({!! Voyager::image($relaxItem->ornament) !!})">
          </div>
          <div class="offer__image"><img src="{!! Voyager::image($relaxItem->image) !!}"/>
          </div>
          <div class="offer__content">
            <h2 class="offer__name">{!! $relaxItem->title !!}
            </h2>
            <div class="content">
            {!! $relaxItem->preview_text !!}
            </div><a class="offer__more" data-idrelax="{{$relaxItem->id}}" data-classmoadl="offer_style-{!! $i2 !!}" href="#" data-featherlight="#offer">подробно</a>
            <div class="offer__bottom"><span class="offer__price">{!! $relaxItem->price !!}</span>
              <button class="call-btn" data-namercall="{!! $relaxItem->title !!}" data-featherlight="#callback">Заказать звонок
              </button>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?/*<section class="section section section_style-{!! $i2 !!}" id="section{!! $i !!}" style="background:{!! $relaxItem->color !!};">
      <div class="section__container container">
        @if($i2%3 && $i2!=1)
        <div class="offer offer offer_style-{!! $i2 !!} offer offer_style-odd" style="background:{!! $relaxItem->color !!};">
        @else
        <div class="offer offer offer_style-{!! $i2 !!}" style="background:{!! $relaxItem->color !!};">
        @endif
          <div class="offer__image" style="box-shadow: 0 0 0 15px {!! $relaxItem->color !!};"><img src="{!! Voyager::image($relaxItem->image) !!}"/>
          </div>
          <div class="offer__content">
            <h2 class="offer__name">
             {!! $relaxItem->title !!}
            </h2>
            <div class="content">
            {!! $relaxItem->preview_text !!}
            </div>
            <a class="offer__more" data-idrelax="{{$relaxItem->id}}" data-classmoadl="offer_style-{!! $i2 !!}" href="#" data-featherlight="#offer">подробно</a>
            <div class="offer__bottom"><span class="offer__price">{!! $relaxItem->price !!}</span>
              <button class="call-btn" data-namercall="{!! $relaxItem->title !!}" data-featherlight="#callback">Заказать звонок
              </button>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?*/
    if($i > 3 && $i2 > 4){
      $i = 0;
      $i2 = 1;
    }else{
      $i++;
      $i2++;
    }
    ?>
    @endforeach
    <section class="section section_reviews">
      <div class="section__container container"><span class="caption">Люди о нас</span>
        <div class="reviews">
          <div class="reviews__items">
            @foreach($reviews as $rev)
            <div class="review">
              <div class="review__author">
                <div class="review__author-photo"><img src="/img/icons/author.png"/>
                </div>
                <span class="review__author-name">{!! $rev->title !!}</span>
              </div>
              <div class="review__text">
                {!! $rev->text !!}
              </div><a class="review__link" href="{!! $rev->src !!}" target="_blank">подробней...</a>
            </div>
            @endforeach
          </div>
          <div class="reviews__links"><a class="btn btn_reviews btn_flamp" href="{!! Voyager::setting('site.flamp_src') !!}" target="_blank">Читать больше<br>Flamp.ru</a><a class="btn btn_reviews" href="{!! Voyager::setting('site.flamp_src_review') !!}">Оставить отзыв</a>
          </div>
        </div>
      </div>
    </section>
    <div class="map" id="map">
    </div>
@endsection
