@extends('layouts.template')
@section('content')
    <section class="section">
      <div class="section__container container">
        <div class="gallery-page">
            @foreach($gallery_type as $gt)
          <div class="g-block">
            <div class="g-block__head"><a class="caption caption_left" href="{!! $gt['src'] !!}">{!! $gt['title'] !!}</a>
            </div>
            <ul class="g-block__list">
              @foreach($gt["gallery"] as $block)
              <li class="g-block__list-block">
                @foreach($block as $itemG)
                <div class="g-block__item"><img src="{!! Voyager::image($itemG) !!}" data-featherlight="{!! Voyager::image($itemG) !!}"/>
                </div>
                @endforeach
              </li>
              @endforeach
            </ul>
          </div>
            @endforeach
        </div>
      </div>
    </section>
@endsection