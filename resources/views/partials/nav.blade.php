@if ($paginator->hasPages())
    <ul class="pagination" role="navigation">
        <!-- {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="pagination__item pagination__item_prev disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                <a aria-hidden="true"></a>
            </li>
        @else
            <li class="pagination__item pagination__item_prev">
                <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')"></a>
            </li>
        @endif -->

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="pagination__item disabled" aria-disabled="true"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="pagination__item pagination__item_active" aria-current="page"><a>{{ $page }}</a></li>
                    @else
                        <li class="pagination__item"><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        <!-- {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="pagination__item pagination__item_next">
                <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')"></a>
            </li>
        @else
            <li class="pagination__item disabled pagination__item_next" aria-disabled="true" aria-label="@lang('pagination.next')">
                <a aria-hidden="true"></a>
            </li>
        @endif -->
    </ul>
@endif