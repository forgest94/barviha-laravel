        <nav class="navigator">
            <ul class="navigator__list">
              @foreach($items as $menu_item)
              <li class="navigator__item navigator__item <?if(count($menu_item->children) > 0):?>navigator__item_drop<?endif;?>"><a class="navigator__link" href="{{ $menu_item->url }}">{{ $menu_item->title }}</a>
                @if(count($menu_item->children) > 0)
                <ul class="navigator__drop">
                  @foreach($menu_item->children as $child)
                  <li class="navigator__item"><a class="navigator__link navigator__link_drop" href="{{ $child->url }}">{{ $child->title }}</a>
                  </li>
                  @endforeach
                </ul>
                @endif
              </li>
              @endforeach
            </ul>
          </nav>