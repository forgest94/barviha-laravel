<?
/*if(!empty($relax->gallery))
$galleryItem = json_decode($relax->gallery);*/
?>
<div class="offer__content offer__content_page container container_offer">
          <div class="offer__info offer__info_page">
            <h2 class="offer__name">{!! $relax->title !!}</h2>
            <div class="content">
            {!! $relax->detail_text !!}
            </div>
            <!-- <a class="offer__more" href="#">подробно</a> -->
            <div class="offer__bottom"><span class="offer__price">{!! $relax->price !!}</span>
              <button class="call-btn" data-featherlight="#callback" data-namercall="{!! $relax->title !!}">
                Заказать звонок
              </button>
            </div>
          </div>
          @if(!empty($relaxImg))
          <div class="offer__gallery">
            <div class="gallery">
              <div class="gallery__image" data-featherlight="{!! Voyager::image($relaxImg[0]) !!}"><img src="{!! Voyager::image($relaxImg[0]) !!}"/>
              </div>
              <ul class="gallery__preview">
                @foreach($relaxImg as $pic)
                <li class="gallery__item"><img src="{!! Voyager::image($pic) !!}"/>
                </li>
                @endforeach
              </ul>
            </div>
            <button class="call-btn" data-close="data-close">Назад
            </button>
          </div>
          @elseif(!empty($relax->image))
          <div class="offer__gallery">
            <div class="gallery">
              <div class="gallery__image" data-featherlight="{!! Voyager::image($relax->image) !!}"><img src="{!! Voyager::image($relax->image) !!}"/>
              </div>
              <ul class="gallery__preview">
                <li class="gallery__item"><img src="{!! Voyager::image($relax->image) !!}"/>
                </li>
              </ul>
            </div>
            <button class="call-btn" data-close="data-close">Назад
            </button>
          </div>
          @endif
        </div>
