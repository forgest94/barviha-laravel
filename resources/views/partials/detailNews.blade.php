@extends('layouts.template')
@section('content')
<?
        if(!empty($news->gallery))
        $galleryItem = json_decode($news->gallery);
?>
<section class="section">
      <div class="section__container container">
        <h2 class="caption">
            {!! $news->title !!}
        </h2>
          <article class="article">
            <div class="content">
              {!! $news->text !!}
            </div>
            @if(!empty($galleryItem))
              @if(count($galleryItem) == 1)
              <div class="article__images article__images_single">
              @else
              <div class="article__images">
              @endif
              @foreach($galleryItem as $pic)
                <img src="{!! Voyager::image($pic) !!}">
              @endforeach
            </div>
            @endif
            <div class="article__footer">
              <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
              <script src="https://yastatic.net/share2/share.js"></script>
              <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter,viber,whatsapp,skype,telegram" data-limit="3"></div>
              <!-- <div class="social"><a href="#"><img src="/img/icons/ok.png" alt="Одноклассники"></a><a href="#"><img src="/img/icons/vk.png" alt="Вконтакте"></a><a href="#"><img src="/img/icons/insta.png" alt="Instagram"></a> -->
            </div>
            <p class="article__publish">
              Опубликовано {{ \Carbon\Carbon::parse($news->created_at)->format('d.m.Y') }}
            </p>
            </div>
          </article>
</div>
</section>
@endsection
