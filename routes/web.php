<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Cache;
Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    Cache::flush();
    return "Кэш очищен.";
});

Route::post('/detailRelax', 'RelaxationController@show');
Route::post('/mailCall', 'RelaxationController@call');
Route::get('/mailCall', 'RelaxationController@call');

Route::group(['prefix' => 'back'], function () {
    Voyager::routes();
});

Route::get('/news/{slug}', 'NewsController@show');
Route::get('/{slug}', 'PageController@show');
Route::get('/', 'PageController@index');
